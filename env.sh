#!/bin/bash

export ROOT_DIR=`pwd`
export ANDROID_NDK=/opt/android-ndk-r10e
export TOOLCHAIN=$ROOT_DIR/toolchain
export SYSROOT=$TOOLCHAIN/sysroot/

export X264_TOOLCHAIN_BUILD_DIR=$TOOLCHAIN/x264
export FFMPEG_TOOLCHAIN_BUILD_DIR=$TOOLCHAIN/ffmpeg

BUILD_DIR=$ROOT_DIR/build
export X264_BUILD_DIR=$BUILD_DIR
export FFMPEG_BUILD_DIR=$BUILD_DIR

export PATH=$TOOLCHAIN/bin:$PATH
export CC=arm-linux-androideabi-gcc
export LD=arm-linux-androideabi-ld
export LD=arm-linux-androideabi-ld
export AR=arm-linux-androideabi-ar

