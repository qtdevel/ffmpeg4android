
1) Install Android NDK to /opt and fix path in env.sh if needed.
2) Run '00.configure.ndk' to prepare NDK toolchain.
3) Run '01.get.x264' to get the latest sources of x264 library.
4) Run '02.configure.x264' to configure x264 library.
5) Run '03.build.x264' to build the library. Result will be in ./build directory.
6) Run '04.get.ffmpeg' to get the latest sources of ffmpeg library.
7) Run '05.configure.ffmpeg' to configure ffmpeg library.
8) Run '06.build.ffmpeg' to build the library. Result will be in ./build directory.
9) Run '10.clean.x264' if you want to clean x264 library. It's optional.
10) Run '10.clean.ffmpeg' if you want to clean ffmpeg library. It's optional.

Directory './build/include' will contain headers.
Directory './build/lib' will contain library files.
